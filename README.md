# Pear Admin Furion

⚡  基于 .Net 5 平台 Furion 生态的落地实践

building ...  :boom: 

# To do something

- [x] 项目起草
- [x] 登录入口
- [x] 用户管理
- [x] 角色管理
- [x] 权限处理
- [x] 权限管理
- [x] 数据字典
- [ ] 基础设施
- [ ] 资源管理
- [ ] 配置中心
- [ ] 系统监控
- [ ] 定时任务
- [ ] 行为日志
 
 1. 用户管理
 2. 角色管理
 3. 权限管理
 4. RBAC分配
 5. 登录 Token 处理
 6. 字典类型列表
# Preview address

[Pear Admin Ant 预览](http://ant.pearadmin.com)

[Pear Admin Layui 预览](http://layui.pearadmin.com)

# Preview image

| Layui  | 预览  |  
|---------------------|---------------------|
| ![](readmes/1.jpg)  | ![](readmes/2.jpg)  |

| Ant  | 预览  |  
|---------------------|---------------------|
| ![](readmes/3.jpg)|  ![](readmes/4.jpg)   |

# Docker Hub

```bash
docker run --name pearadmin -p 5010:80 monksoul/pearadmin:v1.0.0-rc7
```